import Vue from 'vue'
import { library, config } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {
  faCoffee,
  faSearch,
  faHome,
  faChevronRight,
  faChevronLeft,
  faLongArrowAltRight,
  faAngleDoubleRight,
  faArrowRight,
  faAngleDown
} from '@fortawesome/free-solid-svg-icons'
import {
  faTwitter,
  faYoutube,
  faLinkedin,
  faInstagram
} from '@fortawesome/free-brands-svg-icons'
import { faEdit } from '@fortawesome/free-regular-svg-icons'
// This is important, we are going to let Nuxt.js worry about the CSS
config.autoAddCss = false

// You can add your icons directly in this plugin. See other examples for how you
// can add other styles or just individual icons.
library.add(faCoffee)
library.add(faSearch)
library.add(faHome)
library.add(faTwitter)
library.add(faYoutube)
library.add(faLinkedin)
library.add(faChevronRight)
library.add(faChevronLeft)
library.add(faLongArrowAltRight)
library.add(faAngleDoubleRight)
library.add(faEdit)
library.add(faInstagram)
library.add(faArrowRight)
library.add(faAngleDown)

Vue.component('font-awesome-icon', FontAwesomeIcon)
